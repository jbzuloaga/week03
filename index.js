import PageState from "./blogScript/classes/PageState.js";
import PostList from "./blogScript/classes/PostList.js";
import Storage from "./blogScript/classes/Storage.js";
import createEditState from "./blogScript/pages/CreateEditState.js";
import homeState from "./blogScript/pages/HomeState.js";

const storage = new Storage();
const posts = new PostList(storage);
const tag = document.querySelectorAll('.tags');
const listTags = document.querySelector('.list_tags');
const searchButton = document.querySelector('.search_button');
const searchForm = document.querySelector('.search_form');
const searchInput = document.querySelector('.search_input');
const createPost = document.querySelector('.create_button');
const homeButton = document.getElementById('home_button');
const page = new PageState(posts);

export const tags = ['politics', 'tech', 'world', 'science', 'startups', 'design', 'culture', 'popular', 'all'];
export const flags = {
  search: false,
}

let postslits = [
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide1 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['politics', 'tech', 'health']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide2 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['popular', 'science', 'startups']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide3 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['world', 'tech', 'design']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide4 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['design', 'tech', 'health']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide5 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['culture', 'tech', 'health']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide6 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['science', 'tech', 'popular']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide7 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['politics', 'tech', 'health']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide8 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['popular', 'science', 'startups']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide9 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['world', 'tech', 'design']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide10 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['design', 'tech', 'health']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide11 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['culture', 'tech', 'health']},
  {image: "https://thumbs.dreamstime.com/b/white-background-easy-to-remove-aoraki-mount-cook-isolated-highest-mountain-new-zealand-185219601.jpg", title: 'Complete Guide12 to SCSS', body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", tags: ['science', 'tech', 'popular']}
];

if (storage.getList().length === 0 ) {
  storage.saveList(postslits);
}

homeButton.addEventListener('click', () => {
  page.change(new homeState(page));
})
tags.map((tag) => {
  const liTag = document.createElement('li');
  liTag.classList.add('tags');
  liTag.addEventListener('click', (e) => {
    e.preventDefault();
    console.log('click tag');
    page.posts.filterByTag(e.target.innerText.toLowerCase())
    page.change(new homeState(page));
    document.getElementById("jump_to_this_location").scrollIntoView({behavior: 'smooth'});
  })
  liTag.innerHTML = `<a id="${tag}" href="#"><p>${tag.toUpperCase()}</p></a>`;
  listTags.appendChild(liTag);
});

// Filter by task
tag.forEach((element) => {
  element.addEventListener('click', (e) => {
    e.preventDefault();
    page.posts.filterByTag(e.target.innerText.toLowerCase())
    document.getElementById("jump_to_this_location").scrollIntoView({behavior: 'smooth'});
    page.change(new homeState(page))
  })
});

//Search and Input logic
searchButton.addEventListener('click', (e) => {
  console.log('click search button');
  searchForm.removeAttribute('hidden');
  searchButton.setAttribute('hidden', 'hidden');
  searchInput.focus();
});
searchInput.addEventListener('blur', () => {
  searchButton.removeAttribute('hidden');
  searchForm.setAttribute('hidden', 'hidden');
});
searchForm.addEventListener('submit', (e) => {
  e.preventDefault();
  searchInput.value = searchInput.value;
  page.posts.searchByTitle(searchInput.value.toLowerCase());
  document.getElementById("jump_to_this_location").scrollIntoView({behavior: 'smooth'});
  page.change(new homeState(page));
})

//change page to create post
createPost.addEventListener('click', () => {
  page.change(new createEditState(page));
})
page.init();