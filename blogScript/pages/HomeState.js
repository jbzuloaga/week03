import createEditState from "./CreateEditState.js";
import postState from "./PostState.js";

const homeState = function(page) {
  function handlerRenderHighLights(posts) {
    return (`
    <div>
      <div class='posts_Highlist' style='border-bottom: 1px solid #dcdcdc; padding-bottom: 3rem;'>
        ${posts.list.map((post, i) => i < 5 ? renderHighlights(post, i) : '').join('')}
      </div>
    </div>
    `)
  }
  
  function handlerRenderRestPosts(posts){
    return (`
    <div>
      <ul class='posts_Restlist' id='jump_to_this_location'>
        ${posts.filterList.map((post, i) => renderPost(post, i)).join('')}
      </ul>
    </div>
    `)
  }

  function renderPost(post, i) {
    return `
      <li class='post_card'>
        <div class='post_card_image'>
          <img src="${post.image}" alt="post image">
        </div>
        <div class='post_card_title'>
          <h3 data-id='${i}'>${post.title}</h3>
        </div>
        <div class='post_card_body'>
          <p>${post.body}</p>
        </div>
        <div class='post_card_tags'>
          <div class='post_card_tags_container'>
            <h4>tags</h4>
            <ul class='post_tags_list'>
              ${post.tags.map((tag) => `<li class='post_tag'><p>${tag}</p></li>`).join('')}
            </ul>
          </div>
          <div class='post_actions'>
            <a class="edit_button" href="#"><p data-id='${i}'>edit</p></a>
            <a class="delete_button" href="#"><p data-id='${i}'>delete</p></a>
          </div>
        </div>
      </li>`
  }

  function renderHighlights(post, i) {
    return `
    <div class='post_highcard card_${i}' id='task_${i}'>
      <div class='post_highcard_image img_${i}'>
        <img src="${post.image}" alt="post image">
      </div>
      <div class='post_highcard_title title_${i}'>
        <h3>${post.title}</h3>
      </div>
      <div class='post_highcard_body body_${i}'>
        <p>${post.body}</p>
      </div>
      <div class='post_highcard_tags tags_${i}'>
        <h4>tags</h4>
        <ul class='post_tags_list'>
          ${post.tags.map((tag) => `<li class='post_tag'><p>${tag}</p></li>`).join('')}
        </ul>
      </div>
    </div>`
  }

  document.querySelector('#root').innerHTML = `
    ${handlerRenderHighLights(page.posts)}
    ${handlerRenderRestPosts(page.posts)}
  `

  const editButton = document.querySelectorAll('.edit_button');
  const deleteButton = document.querySelectorAll('.delete_button');
  const postButton = document.querySelectorAll('.post_card_title');

  postButton.forEach((element) => {
    element.addEventListener('click', (e) => {
      e.preventDefault();
      // console.log(e.target.dataset.id);
      const showPost = page.posts.getPost(+e.target.dataset.id);
      page.change(new postState(page, showPost))
    });
  })

  editButton.forEach((element) => {
    element.addEventListener('click', (e) => {
      e.preventDefault();
      const editPost = page.posts.getPost(+e.target.dataset.id);
      page.change(new createEditState(page, editPost, +e.target.dataset.id));
    });
  });
  
  deleteButton.forEach((element) => {
    element.addEventListener('click', (e) => {
      e.preventDefault();
      console.log(e.target.dataset.id);
      page.posts.deletePost(+e.target.dataset.id);
      page.change(new homeState(page));
    });
  });

}

export default homeState