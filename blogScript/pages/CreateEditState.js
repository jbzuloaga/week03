import { tags } from "../../index.js";
import homeState from "./HomeState.js";

const createEditState = function(page, data=null, i=null) {
  function renderTags(tag) {
    return `
    <div class='content_tag'>
      <label for="tag_${tag}" class='label_tags ${data && data.tags.includes(tag) ? 'active_label_tag' : ''}' >
        ${tag.toLowerCase()}
      </label>
      <input 
        type="checkbox"
        id="tag_${tag}"
        class="checkbox_tags"
        ${data && data.tags.includes(tag) ? 'checked' : ''}
        hidden
      />
    </div>
    `
  }

  document.querySelector('#root').innerHTML = `
    <h1>${data ? 'Edit Post' : 'Create Post'}</h1>
    <form class='post_form'>
      <div class='post_form_image'>
        <label for='post_image'>Image URL</label>
        <input id='post_image' placeholder="Enter the url's image" value ="${data ? `${data.image}` : ''}" />
      </div>
      <div class='post_form_title'>
        <label for='post_title'>Title</label>
        <input id='post_title' value = "${data ? data.title : ''}" placeholder="Enter the post's title"  />
      </div>
      <div class='post_form_body'>
        <label for='post_body'>Body</label>
        <textarea id='post_body' placeholder='Enter the post's body'>${data ? `${data.body}` : ''}</textarea>
      </div>
      <div class='post_form_tags'>
        <p>Tags</p>
        <div class='tags_options'>
          ${tags.map((tag, i) => i !== tags.length - 1 ? renderTags(tag) : '').join('')}
        </div>
      </div>
      <div class='post_form_button'>
        <button class='submitButton' type='submit' data-id=${data ? '' : 'create'}> ${data ? 'Edit' : 'Create'}</button>
      </div>
    </form>`

  const selectedTagCheckbox = document.querySelectorAll('.checkbox_tags');
  const submitButton = document.querySelector('.submitButton')
  const submitForm = document.querySelector('.post_form');
  selectedTagCheckbox.forEach((element) => {
    element.addEventListener('change', (e) => {
      const selectedTagsLabel = document.querySelector(`label[for="${e.target.id}"]`);
      e.target.checked ? selectedTagsLabel.classList.add('active_label_tag') : selectedTagsLabel.classList.remove('active_label_tag');
    });
  });
  submitForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const postTags = [
      e.target[3].checked ? e.target[3].id.slice(4) : null,
      e.target[4].checked ? e.target[4].id.slice(4) : null,
      e.target[5].checked ? e.target[5].id.slice(4) : null,
      e.target[6].checked ? e.target[6].id.slice(4) : null,
      e.target[7].checked ? e.target[7].id.slice(4) : null,
      e.target[8].checked ? e.target[8].id.slice(4) : null,
      e.target[9].checked ? e.target[9].id.slice(4) : null
    ].filter((element) => element !== null );
    const data = {
      image: e.target[0].value,
      title: e.target[1].value.replace(/"/g, "'"),
      body:  e.target[2].value.replace(/\n\r?/g, '<br />'),
      tags: postTags,
    }
    console.log({data});
    submitButton.dataset.id === 'create' ? page.posts.createPost(data) : page.posts.editPost(data, i);
    page.change(new homeState(page));
  })
}

export default createEditState;