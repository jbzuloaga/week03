const postState = function(page, post) {
  document.querySelector('#root').innerHTML = `
    <article class='post_container'>
      <div class='post_detail_title'>
        <h1>${post.title}</h1>
      </div>
      <div class='post_detail_image'>
        <img src="${post.image}" alt="post image">
      </div>
      <div class='post_detail_body'>
        <h2>Content</h2>
        <p>${post.body}</p>
      </div>
      <div class='post_detail_tags'>
        <h4>Tags</h4>
        <ul class='post_detail_list_tags'>
          ${post.tags.map((tag) => `<li class='post_detail_tag'><p>${tag}</p></li>`).join('')}
        </ul>
      </div>
    </article>
    `
}

export default postState