import homeState from "../pages/HomeState.js";

class PageState {
  constructor(posts){
    this.posts = posts;
    this.currentState = new homeState(this);
  }

  init() {
    
  }

  change(state) {
    this.currentState = state;
  }
}

export default PageState;