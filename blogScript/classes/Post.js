class Post {
  constructor({image, title, body, tags}) {
    this.image = image;
    this.title = title;
    this.body = body;
    this.tags = tags;
  }
}

export default Post
