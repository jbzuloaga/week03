class Storage {
  getList() {
    if(localStorage.getItem('postList')) {
      return JSON.parse(localStorage.getItem('postList'));
    }else {
      return [];
    }
  }

  saveList(list) {
    localStorage.setItem('postList', JSON.stringify(list));
  }
}

export default Storage;
