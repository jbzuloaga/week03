import Post from "./Post.js";

class PostList {
  constructor(storage){
    this.storage = storage;
    this.list = storage.getList();
    this.filterList = this.list;
  }

  getPost(i){
    if (this.list.length === this.filterList.length) {
      return this.list[i];
    } else {
      return this.filterList[i];
    }
  }

  createPost(data){
    const post = new Post(data);
    this.savePost(post);
  }

  savePost(post){
    this.list.unshift(post);
    this.storage.saveList(this.list);
  }

  editPost({image, title, body, tags}, i){
    if (this.list.length === this.filterList.length) {
      this.list[i].image = image;
      this.list[i].title = title;
      this.list[i].body = body;
      this.list[i].tags = tags;
      this.filterList = this.list;
    } else {
      this.filterList[i].image = image;
      this.filterList[i].title = title;
      this.filterList[i].body = body;
      this.filterList[i].tags = tags;
    }
    this.storage.saveList(this.list)
  }

  deletePost(i){
    this.list.splice(i,1);
    this.filterList = this.list;
  }

  filterByTag(tag) {
    if (tag === 'all') {
      this.filterList = this.list;
    }else {
      this.filterList = this.list.filter((post) => post.tags.includes(tag))
    }
    console.log(this.filterList);
  }

  searchByTitle(title) {
    this.filterList = this.list.filter((post) => post.title.toLowerCase().includes(title));
  }
}

export default PostList